# UWB CSSSKL 133 (A)

TABLE OF CONTENTS

[[_TOC_]]

### ZOOM MEETING LINK + TIME
| | |
| :--- | :--- |
| When | Friday 11:30 - 2:00 |
| Where | https://washington.zoom.us/j/9475474016 |

#### SESSION RECORDINGS
Efforts will be made to make lab recordings accessible. Note, however, that this is not a guarantee. If a recording is missed, the course is not responsible for providing the content covered.

- Week 1 - 
- Week 2 - 
- Week 3 - 
- Week 4 - 
- Week 5 - 
- Week 6 -
- Week 7 - 
- Week 8 - 
- Week 9 - 
- Week 10 - 
- Week 11 - 

### ASSIGNMENT LISTING
- [Week 1 - Intro](lab01.md)
- [Week 2 - Intro](lab01.md)
- [Week 3 - Intro](lab01.md)
- [Week 4 - Intro](lab01.md)
- [Week 5 - Intro](lab01.md)
- [Week 6 - Intro](lab01.md)
- [Week 7 - Intro](lab01.md)
- [Week 8 - Intro](lab01.md)
- [Week 9 - Intro](lab01.md)
- [Week 10 - Intro](lab01.md)
- Week 11 - Nothing!

#### DUE DATES
Lab submissions are due at midnight (11:59 PM) the Wednesday following the lab session.

Corrections may be submitted up to one week following the original due date.

#### TURN-IN LOCATION

https://canvas.uw.edu/courses/1444060

#### SPECIAL INSTRUCTIONS
If alterations to the course plan occur, they will be noted here:

- Week 1 - 
- Week 2 - 
- Week 3 - 
- Week 4 - 
- Week 5 - 
- Week 6 -
- Week 7 - 
- Week 8 - 
- Week 9 - 
- Week 10 - 
- Week 11 - 

## COURSE LOGISTICS
### OFFICE HOURS
By appointment (but seriously, Discord me whenever)

### CONTACT
| | |
| :--- | :--- |
| Name | Tim Lum |
| Discord Channel | https://discord.gg/5m48QuM |
| Discord Handle | teabean#2682 |
| Email | tlum@uw.edu |

### COURSE GRADING
Course is graded Credit/No Credit

89.9% required to pass

Grading is based on effort and your ability to submit work in a timely fashion (ship what you got)

4/4 == Strong effort made to solve all problem sets

1-3/4 == Partial problem set solution attempted

0/4 == No turn in or no alteration made to turn in

### INCLEMENT WEATHER POLICY

If labs are cancelled because of inclement weather, the UW Bothell main page will post the announcement at http://www.bothell.washington.edu/ . From this page you can also automatically get current information on campus closures and delays in the event of inclement weather and emergency situations by signing up for text message and email alerts through the UW Bothell Alert System. http://www.bothell.washington.edu/alert/ 

## ERRATA

### MISSION
To provide logical and technical tools to assist you in improving as a computer scientist and software developer.

### DESCRIPTION
This lab section is a companion to the CSS 133 course. 

It provides hands-on practice and experience as well as guides you in implementing programming concepts.

The goal is to get introduced to computational thinking, problem solving, practice-practice-practice writing code and debugging problems, and gain practical experience.

Subjects of this course should help you with other programming aspects of CSS 133 (e.g. homework), as well as reinforce the concepts being taught. 

The topics covered  in this lab will be synchronized with those in the CSS 133 lectures, and are potentially subject to change as a result.  

### LAB REQUIREMENTS  

No requirements are placed on the execution of the lab content; you may develop your code in whatever environment or pipeline you are most comfortable with.

Completed work should be submitted as a text file to Canvas.

## STUDENT CONDUCT

### PLAGIARISM AND CHEATING

You are expected to provide original work based on your own effort for this course. It is your responsibility to know and uphold the Student Conduct Code for the University of Washington (available at www.uwb.edu/students/policies).

Any coursework which is plagiarized or otherwise in violation of the Student Conduct Code will receive a zero and may be referred to the University for further action.

### COURTESY AND TECHNOLOGY

As this is a course on computer science, a computational device will almost certainly need to be used in class. We cannot enforce or police the appropriate usage of your devices, however, and thus politely request that you endeavor to avoid using your phone, tablet, or computer in a manner which might distract you or your peers in the classroom during lab hours.

### PARTICIPATION AND ATTENDANCE

The CSSSKL 132 course grade is derived entirely from the submission of completed labs. While it is possible to succeed in the course on paper, it is our hope that the lab sessions will provide you with a number of habits, intuitions, and skills that will serve you throughout your academic and professional career. As such, even if you are able to complete the labs without attending the session, I advise that you attend.

## ASSISTANCE

### ACCESS AND ACCOMODATIONS

Your experience in this class is important to us, and it is the policy and practice of the University of Washington to create inclusive and accessible learning environments consistent with federal and state law. If you experience barriers based on disability, please seek a meeting with Disability Resources for Students (DRS) to discuss and address them. If you have already established accommodations with DRS, please communicate your approved accommodations to your instructor at your earliest convenience so we can discuss your needs in this course.

DRS offers resources and coordinates reasonable accommodations for students with disabilities.  Reasonable accommodations are established through an interactive process between you, your instructor(s) and DRS.  If you have not yet established services through DRS, but have a temporary or permanent disability that requires accommodations (this can include but not limited to; mental health, attention-related, learning, vision, hearing, physical or health impacts), you are welcome to contact DRS at 425.352.5307 rlundborg@uwb.edu .

### RELIGIOUS ACCOMODATIONS

Washington state law requires that UW develop a policy for accommodation of student absences or significant hardship due to reasons of faith or conscience, or for organized religious activities. The UW’s policy, including more information about how to request an accommodation, is available at Faculty Syllabus Guidelines and Resources. Accommodations must be requested within the first two weeks of this course using the Religious Accommodations Request form available at https://registrar.washington.edu/students/religious-accommodations-request/ (Links to an external site.) 


