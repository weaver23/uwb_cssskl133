---
Document frontmatter
CREATOR:     Tim Lum
SUBJECT:     Hash tables
DESCRIPTION: A laboratory assignment specification for the University of Washington Bothell CSSSKL133 course 
FORMAT:      Markdown
LANGUAGE:    English, Markdown, C++
LICENSE:     BSD 3-Clause
CONTACT:     twhlum@gmail.com
TAGS:        data structures, algorithms, hash tables, nodes, C++
---

# LINKED LISTS

## OVERVIEW

### THIS DOCUMENT...
Describes a laboratory assignment specification covering the topic of Hash Table Data Structures, implemented in C++.

### OBJECTIVES
- Describe the Hashtable Data Structure
  - Hash Table
  - Hash Function
  - Collision Resolution
  - Resizing
  - Time Complexity
- Introduce associated LL Algorithms (Algos)
  - Search / Find / Get
  - Count
  - Insert
  - Delete
  - Copy / Resize
- Introduce bugs and corner cases
  - Collision Rates
  - Duplicates
  - Contiguous Allocation
- Tips and Tricks
  - Hashing Keys Only

### TASKS
Tasks for this assignment:

- [ ] DECLARE AND INSTANTIATE A HASHTABLE
- [ ] DEFINE A HASH FUNCTION
- [ ] SELECT A COLLISION STRATEGY
- [ ] ADD AN ITEM TO THE HASHTABLE
- [ ] FIND AN ITEM IN THE HASHTABLE
- [ ] REMOVE AN ITEM FROM THE HASHTABLE
- [ ] RESIZE THE HASHTABLE
- [ ] DELETE THE HASHTABLE

### TABLE OF CONTENTS
[[_TOC_]]

## CONTEXT

### THE RULES

1. A given input to a hash function always creates the same output
2. The hash output is then scaled to the table size
3. 

### CONVENTIONS

- `TERM` - The definition.
- `KEY` - The current node reflecting one's position in the list.
- `VALUE` - The next node in sequence relative to `CURR`.
- `TUPLE` - The previous node in sequence relative to `CURR`.

```mermaid
graph LR
HEAD(HEAD) --> N1(...) --> PREV(PREV) --> CURR(CURR) --> NEXT(NEXT) --> N2(...) --> TAIL(TAIL)
```

## DESCRIPTION

### WHAT IS A HASH TABLE?
(From [Wikipedia](https://en.wikipedia.org/wiki/Hash_table))

_In computing, a hash table (hash map) is a data structure that implements an associative array abstract data type, a structure that can map keys to values. A hash table uses a hash function to compute an index, also called a hash code, into an array of buckets or slots, from which the desired value can be found. During lookup, the key is hashed and the resulting hash indicates where the corresponding value is stored._

![LinkedList](Img/LinkedList_Overview.svg "Diagramatic representation of a linked list")

### THE HASH TABLE

Basically it's an array.

### HASH COLLISION

Wherein two or more values are stored at the same hash index in the table.

### THE HASH FUNCTION

Hash functions vary a tremendous amount. Fundamentally, the function takes some trait of the input data and returns an array index.

```mermaid
graph LR
INPUT --> HASH_FUNCTION --> ARRAY_INDEX
```

The worst hash function I can imagine would just return 0, indicating that regardless of the input, the element should be stored at the 0th array index of the hash table. Technically, however, this is a valid hash function (it just sucks).

A step up from this, we may take the size of our hashtable and simply modulo divide the value to be stored.

```mermaid
graph LR
INPUT --> HASH[INPUT % SIZE] --> ARRAY_INDEX
```

While an improvement, you can imagine that given certain combinations of inputs and table sizes, the interaction of these numbers may generate more hash collisions than are strictly necessary.

What we would like, ideally, is a mathematical function that almost randomly places any given input on the table (but is still deterministic) - that is, we would like a uniform distribution of index assignments.

Fortunately, this problem has actually already been solved six ways to Sunday. Excellent hashing algorithms already exist including [SHA-256](https://en.wikipedia.org/wiki/SHA-2) and [xxHash](https://github.com/Cyan4973/xxHash/blob/dev/README.md), though it should be noted that cryptographic hashes - while excellent from a collision avoidance standpoint - will tend to perform slower than less secure hash algorithms that are optimized for speed.

Doubly fortunately, if you're using somebody else's hash table, you really shouldn't have to worry about which hash algorithm is being used under the hood. If you are selecting (or heaven forbid, writing) your own hashing algorithm, chances are good that you're addressing an issue that teams of developers smarter than us have already looked at.

### COLLISION RESOLUTION: SEPARATE CHAINING (LINKED LISTS)

TODO

### COLLISION RESOLUTION: OPEN ADDRESSING (PROBING)

TODO

### RESIZING

Because collision resolution takes progressively longer as the collisions build up, you can hopefully visualize that the benefit of using a hash table lessens the more collisions we have. At the extreme, a hash table of size 1 is basically just a linked list or (in the case of open addressing) an endless loop of probing.

To resize a hash table, we must allocate a new, larger memory space, then re-insert all prior elements to the new table. Effectively, we must rehash our information.

All told, there are several elements to consider if we are to attempt to arrive at an ideal solution:

- Computational cost to rehash
- Collision rate
- Computational cost to handle collisions

However, nobody in their right mind is going to ask you to precalculate a theoretical ideal, particularly since these factors are likely to change in any given operating environment. The more sane way to approach this problem is to select:

- A maximum density of the hash table (some ratio of elements to available indices)
- A resizing factor (usually 2x)

You may then instrument your program and execute some real-world tests to tune for the best density and resizing factor.



### TIME COMPLEXITY

Hash tables are notable in that for most of their operations, they are recognized as being O(1) actions. That is, regardless of the data collection's size, most of its actions are constant-time. This is basically the holy grail of computation.

The tradeoff comes in that hash tables must utilize sometimes comical amounts of memory space to accomplish this feat.

## IMPLEMENTATION

### THE TOOLS
The Node class (minimally):
```cpp
class Node {
  Node* nextPtr;
  int value;
}
```

The Linked List class (minimally):
```cpp
class LinkedList {
  Node head;
  
  LinkedList() {
    this->head = new Node();
  }
}
```

The Driver:
```cpp
int main( ) {
  LinkedList myList = new LinkedList();
}
```

## TASKS

Using the following (optional) script:

compile.sh
```
rm lab.out
g++ --std=c++11 laboratory.cpp -o lab.out
./lab.out
```

### DECLARE AND INSTANTIATE A NODE

Driver:
```cpp
#include <iostream>
#include <iomanip>

struct Node {
  // TODO: Declare and implement the Node class
};

int main( int argc, char* argv[] ) {
  Node* n_ptr = new Node( 123 );
  std::cout << n_ptr->value << std::endl;
  std::cout << std::boolalpha << (n_ptr->next == nullptr) << std::endl;
} // Closing main()
```

Expected output:
```
123
true
```

### DECLARE AND INSTANTIATE A LINKED LIST

Driver:
```cpp
#include <iostream>

struct Node {
  int value;
  Node* next;
  Node( int arg ) {
    this->value = arg;
  }
};

struct LinkedList {
  // TODO: Declare and implement the Linked List class
  Node head;
};

int main( int argc, char* argv[] ) {
  LinkedList L = LinkedList();
  L.head->value = 123;
  std::cout << L.head->value << std::endl;
} // Closing main()
```

Expected output:
```
123
```

### ADD NODES TO THE LINKED LIST


### TRAVERSE ACROSS THE LINKED LIST


### INSERT A NODE AT A POSITION

### DELETE A NODE WITH A GIVEN VALUE

### COPY THE LINKED LIST

### DELETE THE LINKED LIST

## BUGS + CORNER CASES

### BROKEN CHAIN

```mermaid
graph LR;
  A --> B --> C;
  D --> E --> F;
```

If, for whatever reason, the linked list is ever 'broken' (that is, the 'next' reference of a node is redirected inappropriately), all subsequent nodes in the list will be lost. Once detached, this lost memory is essentially unrecoverable and becomes known as a 'memory leak' - allocated memory that cannot be used, accessed, or deleted.

### LOOPS / CYCLES

On the odd chance that one of the nodes points back to a prior node in the LL, a structure called a 'cycle' will occur. In this, you may note that our handy iterative loop of `while( node->next != nullptr )` will in fact just go on forever.

```mermaid
graph LR;
  A --> B --> C --> D --> E --> C;
```

But how do you tell the difference between just a very long, similar looking list (think DNA sequences) and a true cycle? The solution to this problem is colloquially called the 'Tortoise and Hare' algorithm and formally as '[Floyd's Algorithm](https://en.wikipedia.org/wiki/Cycle_detection#Floyd's_Tortoise_and_Hare)'. In it, two pointers traverse the list at differing rates. As a result, the faster pointer will eventually either reach the end or 'lap' the slower pointer. Should the two pointers ever indicate the same node, you can be well assured that a loop exists.

### INTERSECTION / CONVERGENCE

```mermaid
graph LR;
  A --> B --> C --> D --> E;
  F --> G --> C;
```

### RECURSIVE DELETION

A recursive callstack may be used to delete elements from the end of the Linked List. In testing, this will probably work just fine and yields code that is succinct and elegant.

```cpp
void delete( Node* tgt ) {
  if( tgt->next != nullptr ) {
    delete( tgt->next );
  }

  delete tgt;
}
```

However, doing so introduces a bug. Namely, the maximal linked list length that may be deleted in this fashion is equal to the OS-enforced depth of the call stack that you may use. If this is exceeded, you will create a stack overflow and your program will terminate in an uncontrolled fashion.

This bug is particularly insidious in that it will likely not appear during testing or evaluation, as the length of test lists are likely to be significantly smaller than the maximum callstack depth.

Because of this, an iterative solution which deletes from the front is preferred to a recursive solution that deletes from the back.

## TIPS + TRICKS

### NODE DESTRUCTOR MESSAGE

Appending a `cerr` or `cout` message to the destructor may assist in debugging which nodes are deleted when and where (or, as is the case when hunting memory leaks... which nodes are -not- being deleted).

### LAZY DELETION

"Lazy Deletion" is a trick that can be used in most data structures. In it, the node definition is modified to include an `isDeleted` boolean (or similar signal). When the data structure is queried as to whether an object is "in the set" or not, it will look for the value as normal and check for both the presence of the node in question as well as its deletion flag.

Why the added complexity?

In situations where memory is not a concern, lazy deletion often requires less time to perform than the restructuring and background processes needed to properly deallocate memory. Often, a performance improvement can be gleaned by leaving the element in situ, but flagging it as being "deleted". While the benefits are debateable in a Linked List situation, the advantages of lazy deletion become more apparent in data structures where removal may demand a reshuffling of a large number of data elements.

Lazy deletion also has the ancillary benefit that if your program is being evaluated for memory leaks, there is a chance that improperly deleted resources will not show in the final memory report since those resources are technically still in use and not lost or detached.

## REFERENCES

[1]
```json
@startjson
{
  "VALUE": "123",
  "NEXT":
  {
    "VALUE": "456",
    "NEXT":
    {
      "VALUE": "789",
      "NEXT": [ "!" ]
    }
  }
}
@endjson
```

[2] If applicable
