# WEEK 1 - WELCOME

## INDEX
[[_TOC_]]

## TASK CHECKLIST
- [ ] [Task 1](#task-1)
- [ ] [Task 2](#task-2)
- [ ] [Task 3](#task-3)
- [ ] [Task 4](#task-3)
- [ ] [Pre-lab 1](#pre-lab-1)
- [ ] [Pre-lab 2](#pre-lab-2)

## ADMINISTRIVIA

### FOLLOW-UPS
None! Because this is the first week!

### GRADES
TL:DR
- 90% or higher needed to pass, graded Credit/No-Credit (CR/NC)
- Laboratory section must be passed together with the lecture section
- Labs are graded on effort, not correctness
  - Pre-lab activity == 1 point
  - Lab activities   == 3 points

#### TURNING IN WORK
Submit responses to the task/prelab checklists as a text document to Canvas.

Pseudo-bonus points if you create a Git repository and host your files for the class there (you can ever fork this document into your own repo!).

#### MAKEUP / LATE WORK
##### LAB ACTIVITY:
Didn't receive full marks? Check the assignment comment on Canvas for why the point(s) was/were missed, make the correction, turn in by the following week.

##### PRE-LAB ACTIVITY:
Purpose is to drive consideration of the lab prior to class so you have narrative context. As such, can't really do make up for this, sorry!

### OFFICE HOURS VOTING:
None this quarter; by appointment only but more generally any hour of the day on Discord

### COURSE PULSE:
For any given week (N) of the quarter, the rhythm will be as follows:

![Course pulse](Img/course_pulse.jpg)

#### EXCEPTIONS + SPECIAL CASES
##### WEEK 1 - FIRST WEEK
No pre-lab activity, of course, so enjoy the point freebie!

##### WEEK 10 - LAST WEEK
No new content BUT lab capstone and review for your lecture final exam; class will still be a go!

##### WEEK 11 - FINALS
No lab session or content. Office hours will still be active as normal for last questions/answers.
Final lab makeup (week 10) to be turned in End of Day (EoD)

### ON EXCUSES
<dl>
<dt> "Canvas wouldn't accept my submission" </dt>
<dd> Submit -something- at the end of every laboratory period to get yourself the 1 week extension. There is no point penalty for doing this. If you tell me that Canvas had a problem -after- the due date without having turned anything in initially, it'll be nice to know but I still can't give credit for that. </dd>

<dt> "That's not what I turned in" </dt>
<dd> Canvas has a download button; it is part of your responsibility to re-download your submission and inspect it to ensure that the assignment repository contains your work. </dd>

<dt> "I just left out the pre-lab" </dt>
<dd> Which means I'm contractually obligated to leave out one point. `¯\_(ツ)_/¯` </dd>

<dt> "I turned in work from a different class" </dt>
<dd> I… don't even know how you did that. Don't do that. </dd>

<dt> "The government has collapsed and I need to join my local resistance cell" </dt>
<dd> Just don't do anything heroic and get your work in by the end of the quarter. I'll see what I can do for credit. </dd>

<dt> +silent failing intensifies+ </dt>
<dd> You do you. </dd>
</dl>

## PREFACE - WHY ARE WE HERE?
Welcome to the CSSSKL133 lab section! Having survived through 132, you're probably pretty familiar with what's going on here, so I'll spare you the rehash from last quarter. This is just the continuation of that series.

One thing that is worth mentioning; last quarter we were focused largely on the mechanics of how C++ and computer programs worked (the rules of the game, so to speak). This quarter, we are going to be focused on how we do useful work with those rules.

## PERFORMANCE
Much of the work we do in programming (and computing more generally) has to do with the creation of performant systems. High performance is good, low performance is bad; a simple enough set of rules to follow. However, ask a dozen different individuals surrounding your software system what 'performance' is, and you're likely to get twice as many answers back.

Performance boils down to any metric that an individual or organization can measure. The higher up in an org you go, the more this question tends to center around operating budgets and dollar values, while the base of an organization pyramid tends to have much more varied concerns.

##### TASK 1
> List a few aspects of 'performance' with regards to computer systems and software. <br>
> [Back to checklist](#checklist)

As most software development has to do with the correct assembly of software libraries and components (we're not going to write most of the code ourselves, honestly), software architects will mostly understand performance to mean something related to how well-supported libraries are and whether those libraries perform the tasks needed and integrate into the larger system smoothly. How much friction does the use of an application cause and what is the likely outcome of incorporating a technology over the next five to ten years?

But for the people writing software (the engineers), performance begins to mean some very specific things, and these specificities are what we will be examining this quarter. To an engineer, performance tends to translate towards:

- **Time Complexity** - How quickly does a program run?
- **Space Complexity** - How much memory does a program need?
- **Maintainability** - How easily can we fix/change the program?

### MAINTAINABILITY
This is an ongoing effort that is extremely difficult to quantify. While the time and space performance of a program can be measured, there is practically no way to gauge how easy a program is to maintain without access to its source code and development environment. For whatever reason, these questions also tend not to be asked in interviews, so we will unfortunately not be spending much time on this aspect of software performance.

However, if you're interested, it includes attempts to answer questions like:

- How do you know your code works as intended?
- If a component in your system changes, do you know if the system got better or worse?
- Do classes and functions have limited, well defined responsibilities?
- Are the interfaces between logical elements well defined and enforced?
- Are variables isolated from each other?
- Is the system well-documented and is this documentation accessible and updateable?

Honestly, we're going to glaze over most of this this quarter, but don't let that diminish the importance of this performance aspect in real-world software development.

### SPACE COMPLEXITY
<dl>
<dt> Space Complexity </dt>
<dd> A measure of the amount of working storage (memory) a program needs. Often, this is in reference to how the space requirements grow in the worst case as the size of the program inputs increase. </dd>
</dl>

One of the main focuses of performance discussions is 'Space Complexity'. These discussions tend to center around the question of how much memory a program uses in order to accomplish a given task. As it so happens, some problems are easier to solve when you have a lot of memory to solve them in, and solutions can be significantly confounded when the amount of memory available becomes limited.

Generally, memory is not a primary concern since the costs associated with this resource have fallen so quickly.

### TIME COMPLEXITY
![Time](Img/time_complexity.jpg)

<dl>
<dt> Time Complexity </dt>
<dd> A measure of the amount of operations (time) a program needs. Often, this is in reference to how the time requirements grow in the worst case as the size of the program inputs increase. </dd>
</dl>

The other major focus (and the one we will be devoting most of our efforts to this quarter) is '**Time Complexity**'. In determining the time complexity of a software function or system, we are mostly wondering about how long the system takes to arrive at an answer. Better solutions will tend to use less time to arrive at an answer, and this just reflects an efficiency of our work strategy towards the problem.

### TIME-SPACE TRADEOFF
While not necessarily a universal truth, you will tend to find that solutions which are optimized for either time or space will tend to do so at some cost to the other. The fastest computational solutions will tend to use a lot of memory, while efforts to preserve memory space often incur additional processing steps to achieve this. Rarely, some solutions will be both time and space optimized (or very close to), but these form the exceptions rather than the rules.

### DATA STRUCTURES AND ALGORITHMS
![Library](Img/library.jpg)

This is a really common term in computation, and reflects the two core challenges related to time and space in computers. "Data Structures" are a reflection of how we physically or logically organize our data, while "Algorithms" are the steps or processes we use on data to accomplish some goal.

Often, Data Structures and Algorithms are mentioned together since the strategy we use to organize data tends to have a direct relationship to the means by which we work with it. However, the two are not synonymous with each other and should be understood as two distinct concepts.

- **Data Structure** - A data organization, management, and storage format
- **Algorithm** - A sequence of instructions, usually to solve a problem or yield a desired result

> Modern algorithms form the crown jewels of computer science and were hard-won over many centuries (or even millenia, in some cases). They are often non-intuitive, abstract, and esoteric. Don't be put-off or frustrated if it takes a significant amount of time to become familiar with them.

##### TASK 2
> Given a sorted array of names, describe a strategy (algorithm) for finding an individual within that collection. <br>
> [Back to checklist](#checklist)

##### TASK 3
> Describe  a search strategy in the event that the array is not sorted. <br>
> [Back to checklist](#checklist)

## LINUX

On the topic of performance, while most of the consumer computer market trended towards fancy, fully colored, graphical user interfaces with icons and sounds and animations (c.1970 onward), there remained a subset of the computational world that just didn't care for these niceties. Servers, supercomputers, and the devices of people focused on getting computational work done just didn't have the clockcycles to spare on features designed for human comfort.

These systems were largely served by the Unix and Unix-Like operating systems, which eschewed polish, safety, and user guidance in favor of dedicated, raw computational potential. The [development of these operating systems](https://en.wikipedia.org/wiki/History_of_Unix) largely took a back seat in the public consciousness (not in least part because they tended to be developed as open source projects with no vested marketing interest in their proliferation).

### INSTALLATION

As students, we aren't particularly interested in the performance of Unix/Linux systems, but we do want you to have experience using them. If you end up interacting with any back-end computer systems in the future, there is a fairly high chance that they'll be Unix or Linux.

If you are on a Windows machine, you can acquire an integrated Linux system (the 'Windows Subsystem for Linux' or 'WSL') by the following procedure:

https://docs.microsoft.com/en-us/windows/wsl/install-win10

If you are on a Mac, a Unix shell should already be present, and may be accessed by activating the 'Terminal' utility.

> Note:
> If you're on a Mac, sorry, I have no idea how to get you up and running, but I imagine Google can get you sorted out. Hit me up on Discord and we can walk through it if you need the assistance.

### USAGE

Once you're in a terminal environment, one of the first things you should notice is that there are approximately no graphics. Unless you ask the terminal specifically to start rendering graphics, everything here is going to be text-based.

![terminal](Img/terminal.jpg)

Commands are issued by typing in the name of the command or process you would like to run, then following that with any arguments or modifiers to the command. You may have played around a little bit last quarter with sending commands into a program from the execution line; these are called **command line arguments**, and will become a regular feature of your careers moving forward.

Command line instructions will (in this course) be preceded by the '`$`' character.

```
$ <some command goes here>
```

A few things about command line arguments:

- Usually the order of arguments and options doesn't matter. Usually.
- Options are usually (but not always) preceded by a '`-`'
- There is no standard for how options are defined for any given program
  - Usually `-v` tells you the program version
  - Usually `-h` shows you the help file
  - Usually `-o` corresponds to some sort of output specification
- `$ man <program_name>` usually shows the program's manual

However, because these arguments are processed by sending the argument character string into the program, it is actually up to the program developers to determine how their software is going to deal with whatever string you feed in.

### WRITING A PROGRAM
This works the same in Linux as it does in any other OS. The objective is just to get your code saved as a text file.

```cpp
#include <iostream>

int main() {
  std::cout << "Hello, World!" << std::endl;
  return 0;
}
```

### COMPILING THE PROGRAM
Once the program logic has been authored, you will often need to 'build' or 'compile' the source code into an executable. Last quarter, we were perhaps able to ignore this step, but this quarter we'll want to be able to do this ourselves.

Normally, you would call a compiler program (we'll just call it the **compiler**), and its job would be to translate a source code text file into much more specific machine-code instructions that your processor could actually use to perform computations with.

```plantuml
SOURCE_CODE -> COMPILER
COMPILER -> EXECUTABLE
```

One such compiler is **GCC** (short for GNU Compiler Collection), a software program that is made available on both Linux and Unix. To use it, we would normally invoke the program with:

```
$ gcc <source code filename to compile>
```

However, it's entirely possible that you don't have GCC on your computer yet. So how do we get it?

### DOWNLOAD THE COMPILER (OR ANYTHING ELSE)
Without a GUI and web browser, it is not immediately apparent how you can get new software into your Linux environment. Most Linux installations will come bundled with a thing called a **package manager** (PM). This is a software program that behaves much like an index, keeping track of what software you have installed on your system and (more importantly) what software systems are available on the internet at large.

There are several PMs on the market, but the one referenced in this class will be a thing called **APT** (for Advanced Package Tool). To ask it to acquire a package, we would just call:

```
$ apt-get <package name>
```

If you attempt this, however, you'll probably get an error for at least two reasons:

1. APT's local registry of packages is out-of-date
2. APT doesn't have administrator privileges to download and install things to your system

To provide administrator privileges, we preface the command with 'sudo' (short for 'SUperuser DO'), like so:

```
$ sudo apt-get <package name>
```

You'll be prompted for your Linux password, and this will grant Sudo privileges to the process for a short duration (around five or ten minutes).
Next, we need to update APT's registry so that we're sure to receive the latest version of whatever package we go to install. This can be done by:

```
$ sudo apt-get update
```

Performing this action updates our local registry, so now APT knows what the latest version of things are supposed to be. This also tells it what local packages are out-of-date or in need of an update, but it will not perform that action unless explicitly instructed to do so. The upgrade of all packages can be performed with:

```
$ sudo apt-get upgrade
```

`Upgrade` will take significantly longer to complete depending upon how many packages you have and how many downloads APT must perform to bring everything up to a good present state. Don't perform this action close to a deadline, as it can take hours to complete and interrupting it is extremely dangerous. 

Lastly, we can go get the GCC compiler that we're interested in. Before doing this, first check to ensure that we don't already have it with
`$ gcc -v`

The '`-v`' flag tells GCC that we just want to know its version. If GCC is installed, it will start, process the `-v` flag, and provide back the information we requested. If it's not, you'll get an error that no process called '`gcc`' could be found.

If GCC is installed, you may want to upgrade it. GCC specifically may be upgraded using:

```
$ sudo apt-get upgrade gcc
```

Normally, you would install the GCC package using something like:

```
$ sudo apt install gcc
```

However, GCC is actually included with a bundle of other software and these other utilities are ones you may want in the future.

```
$ sudo apt install build-essential
$ sudo apt-get install manpages-dev
```

Whichever path you choose, GCC should now be installed, and you can now compile your program!

### COMPILING THE PROGRAM (REDUX)
Now that we have GCC and a program, let's compile it. Navigate to the directory that your program is stored in using 'cd' (Change Directory):

| | |
| :--- | :--- |
| `$ cd ..` | Go up one directory level |
| `$ cd <Directory>` | Change to the `<Directory>` |
| `$ cd ~` | Go to the Home directory |
| `$ cd <Absolute Path>` | Go to the directory located at `<Absolute Path>` |

Once you've reached the correct directory, you can check to ensure that you're looking at your program using:

```
$ ls
```

Which will LiSt the contents of the directory. If you can see your program, you can now invoke GCC to compile it into an executable using:

```
$ g++ *.cpp
```

'`*.cpp`' is a simple regular expression which informs the compiler to find (match) against all 'Wildcard' (*) .cpp files. If there are specific .cpp files to use (that is, you may have multiple projects in the same directory and don't actually wish to compile ALL of them), you can specify their filenames directly in lieu of the wildcard character (*).

This should create a new file in the directory (check with '$ ls') called 'a.out'. That output file is actually the machine instruction result derived from your source code, which you may run using:

```
$ ./a.out
```

Two final details to this process. First, 'a.out' isn't a great name. You can add an output flag option to the g++ command to create a custom output filename. Note that '.out' is basically meaningless (as almost all file extensions sort of are):

```
$ g++ *.cpp -o <Output name goes here>
```

Second, you may need to compile based upon a specific version of the C++ standard. If so, you can set a version flag as follows (using C++ 11 as an example, since that is common in the UWB curricula):

```
$ g++ --std=c++11 *.cpp
```

###### TASK 4
> In plain English, describe what the following command will do: <br>
> `$ sudo g++ --std=c++17 tlum_assignment1.cpp -o myAwesomeProgram` <br>
> [Back to checklist](#checklist)

## PRE-LABS FOR WEEK 2
###### PRE-LAB 1
> A Shell Script is a text file that just contains the commands you would normally type into a terminal. When executed, the Linux OS will read the script, line-by-line, attempting to execute each line as if it were a command. <br>
> Consider briefly why and how this capacity might be useful to you as a software developer. <br>
> [Back to checklist](#checklist)

###### PRE-LAB 2
> At present, USB devices self-report to the computer they connect to, informing them of the device's nature and functionality. A keyboard, for instance, will tell the computer that it is, in fact, a keyboard, after which point the computer will accept inputs from the device as if they were keystrokes. <br>
> Based upon your answer to Pre-Lab I, explain why this is actually a terrible, terrible idea. <br>
> [Back to checklist](#checklist)
