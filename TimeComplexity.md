
### ORDER OF (O)
Right off the bat, however, you can immediately see that arriving at a fixed answer to these questions is an extremely difficult (if not impossible) question. There are too many confounds to really begin to engage with the performance metrics we've decided upon.

- What if the data comes to us in an ideal state?
- What if we're running this on a really old processor?
- What if the data elements have to be read off a disk drive rather than RAM?
- What if we do the operation at 3:00 in the AM versus at peak traffic hours?
- And so on and so forth.

In an effort to simplify this discussion, engineers have decided to omit most confounds from the equation and have an implicit agreement to generally consider:
- Very large bodies of data to process
- The worst-case scenario with regards to that data
- The count of operations instead of the actual time to perform

By using these assumptions, the system or strategy in question can be examined and compared against its peers without regard given to variations in the input size or the characteristics of the hardware being used. Constant costs (like the program size and loadtime) will also amortize out to basically nothing if the data processing job is big enough.

Are these perfect and safe assumptions to make? Absolutely not, but it's the point from which we will start.

With tightly controlled operating assumptions, we may begin to compare strategies against each other as being on an order (`O( )`) relative to the input or task size. What's more, this notation can be applied to either time or space, so we can write similar expressions for either.

### HOW MANY TIME COMPLEXITIES ARE THERE?
Technically, probably an infinite number of possible time complexity combinations could exist. In 99.999% of applied computer science, however, there are about five that we care about.

| Time Complexity | Name | Description |
| :--- | :--- | :--- |
| $`O(1)`$ | Constant Time | Operation takes a fixed amount of time irregardless of the sample size |
| $`O(logN)`$ | Logarithmic Time | 
| $`O(N)`$ | Linear Time | Operation takes a time proportional to the sample size |
| $`O({N^2})`$ | Quadratic Time | Operation takes a time proportional to a power of the sample size |
| $`O(N!)`$ | Factorial Time | Somehow, it's even worse than quadratic time |

For a more exhaustive list of the primitive time complexities, consult the [Wiki on the subject](https://en.wikipedia.org/wiki/Time_complexity)

### PRACTICE

Assuming an input body `N`, write an expression of the following:

A task's time or space takes...
- A) On the order of the input size.
- B) On an order of twice the input size.
- C) On an order of the square of the input size.
- D) On the order of the square root (logarithm) of the input size.
- E) On a constant order regardless of the input size.

### VARIANTS ON O() NOTATION
So a few things bear mention at this point:
- 'N' is the most common variable used to reflect the input size; but it can be any variable designation ("X", "Y", "Foobar", etc.)
- There can be more than one variable that affects the processing time
- There are other notations for other assumptions:
  - $`o(N)`$ - "Little 'O' of N" - ???
  - $`\Omega(N)`$ - "Omega of N" - ???
  - $`\Theta(N)'$ - "Theta of N" - ???
  - $`O(N)`$ - "Big 'O' of N" - ???
- If you find the formal definitions of these, you may note that the actual "Worst Case" identifier is not, actually, Big-O.
- There is not consensus on what these terms mean.
- Do not try to commit this to memory until you are in a context where it matters and you and a peer need to arrive at consensus.

## SO... WHY DO WE CARE?
Last quarter, we were concerned primarily with the correctness of execution; were you able to write a program that could generate an appropriate result from a known input? This quarter, we are additionally concerned with managing the real-world resources that computers make available to us - namely (processor) time and (memory) space.

To work efficiently with these resources is to establish a respect for the limitations that you will encounter in any business venture that requires computation. More than that, as you'll find this quarter, there are some strategies for solving problems that will _never_ be feasible, even if you had all the hardware budget in the world. Some problems can only be addressed via a specific strategy.

### BUT REALLY... WHY DO WE CARE?
Assume you have a phonebook and are tasked with finding an entry in the book. How could you do it?

More to the point of this quarter, what are the fewest number of operations we could perform to accomplish this task?

If the book were organized differently, could we find the name in 1 operation?
